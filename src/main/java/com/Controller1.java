package com;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class Controller1 {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcome() {
	System.out.println("Application Startup Welcome Page");
	return new ModelAndView("index");
	}
}
